def add_value(a,b):
    return a+b
    
def multiply_value(a,b):
    return a*b

if __name__=="__main__":
    a,b = 2,5
    res = add_value(a,b)
    new_res = multiply_value(res,2)
    print(new_res)