import unittest
import main

class UnitTest(unittest.TestCase):
    def test_add_value_positive(self):
        self.assertEqual(main.add_value(2,3),5)
    
    def test_add_value_negative(self):
        self.assertEqual(main.add_value(-2,-9),-11)
    
    def test_multiply_value_positive(self):
        self.assertEqual(main.multiply_value(2,3),6)

if __name__=='__main__':
    unittest.main()